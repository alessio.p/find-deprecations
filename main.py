# Copyright (c) 2014-2024 Zuru Tech HK Limited, All rights reserved.

import logging
import sys
from dataclasses import dataclass

from find_deprecations.modules.parsing import (
    get_deprecated_warnings,
    parse_gitlab_changes,
)
from find_deprecations.modules.gitlab_utils import GitlabApi


def _help():
    message = """
    This is a program that reads the compilation output looking for deprecation warnings and check if they are added in the range of two commits.

    Usage: python find_deprecated_functions.py FILE_PATH REPO_ABS_PATH REVIWERS_FILE_PATH PROJECT_ID CI_MERGE_REQUEST_IID GITLAB_TOKEN

    FILE_PATH: the path of the file that contains the build output. Only gcc format is supported
    REPO_ABS_PATH: the absolute path of the git repository
    REVIWERS_FILE_PATH: the path of the file that contains the ids of the people that will be assigned as reviewer to the merge request in case of warnings
    PROJECT_ID: Gitlab project ID
    CI_MERGE_REQUEST_IID: Gitlab merge request ID
    GITLAB_TOKEN: token to access to Gitlab by REST API"""
    print(message)


@dataclass
class InputArgs:
    file_path: str = ""
    repo_path: str = ""
    reviewers_file_path: str = ""
    project_id: int = -1
    merge_request_id: int = -1
    access_token: str = ""


def _check_arguments(args: list) -> InputArgs:
    if len(args) != 6:
        _help()
        sys.exit()
    return InputArgs(
        file_path=args[0],
        repo_path=args[1],
        reviewers_file_path=args[2],
        project_id=int(args[3]),
        merge_request_id=int(args[4]),
        access_token=args[5],
    )


def _read_reviewers_to_add(filename: str) -> list:
    usernames = []
    with open(filename, "r", encoding="utf-8") as file:
        usernames = [line.rstrip() for line in file]
    return usernames


def main():
    logging.basicConfig(level=logging.ERROR)
    input_args = _check_arguments(sys.argv[1:])
    deprecated_warnings = get_deprecated_warnings(
        input_args.file_path, input_args.repo_path
    )
    print("Build output file: " + input_args.file_path)
    print("Local repository path: " + input_args.repo_path)
    print("Gitlab project id: " + str(input_args.project_id))
    print("Gitlab merge request id: " + str(input_args.merge_request_id))
    gitlab_api = GitlabApi(
        input_args.access_token, input_args.project_id, input_args.merge_request_id
    )
    if not gitlab_api:
        sys.exit(1)
    changes = parse_gitlab_changes(gitlab_api.get_merge_request_changes())
    errors = []
    print("Deprecation warnings found in commits range:")
    for warning in changes:
        if warning in deprecated_warnings:
            errors.append(warning)
            print("    " + warning.file_rel_path + ", " + str(warning.line))
    print(
        "There are "
        + str(len(deprecated_warnings))
        + " deprecation warnings. Found in the range of specified commits: "
        + str(len(errors))
    )
    if len(errors) > 0:
        reviewers = _read_reviewers_to_add(input_args.reviewers_file_path)
        if gitlab_api.update_merge_request_reviewers(reviewers):
            print("Add reviewers to MR:")
            for name in reviewers:
                print("    " + name)
    sys.exit(len(errors))


if __name__ == "__main__":
    main()
