# Copyright (c) 2014-2024 Zuru Tech HK Limited, All rights reserved.

from collections import namedtuple

FileAndLineTuple = namedtuple("FileAndLineTuple", ["file_rel_path", "line"])

##### Maybe Monad #####
#  https://wiki.haskell.org/All_About_Monads#:~:text=In%20Haskell%20a%20monad%20is,m%20b)%20%2D%3E%20m%20b%20).
#
# It's like an optional with a member function: map(functor, args)
# that applies functor(optional, args), only if optional has a valid value.
# Just is the derivation of Maybe with a valid value.
# Nothing is the one that hasn't.
# This avoid boilerplate code that requires to check optional value before apply a function


class Maybe:
    def map(self, func, *args) -> "Maybe":
        pass

    def value(self):
        pass

    def __bool__(self):
        return self.value() is not None


class Nothing(Maybe):
    def map(self, func, *args) -> "Maybe":
        return Nothing()

    def value(self):
        return None


class Just(Maybe):
    def __init__(self, value) -> None:
        self._value = value

    def map(self, func, *args) -> "Maybe":
        return func(self._value, *args)

    def value(self):
        return self._value
