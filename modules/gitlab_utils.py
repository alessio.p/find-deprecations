# Copyright (c) 2014-2024 Zuru Tech HK Limited, All rights reserved.

from collections.abc import Callable
import logging
import gitlab

from find_deprecations.modules import data


def exceptions_decorator_factory(default_value):
    def exceptions_decorator(function: Callable):
        """Decorator to handle gitlab exceptions."""

        def inner():
            return_value = default_value
            try:
                return_value = function()
            except (
                gitlab.GitlabAuthenticationError,
                gitlab.GitlabGetError,
                gitlab.GitlabUpdateError,
            ) as error:
                logging.error(error)
            return return_value

        return inner

    return exceptions_decorator


class GitlabApi:
    """Class that wraps gitlab-python api."""

    def __init__(
        self, access_token: str, project_id: int, merge_request_id: int
    ) -> None:
        @exceptions_decorator_factory(default_value=(None, None, None))
        def init_api() -> tuple:
            connection = gitlab.Gitlab(private_token=access_token)
            project = connection.projects.get(project_id)
            merge_request = project.mergerequests.get(merge_request_id)
            users = connection.users
            return (project, merge_request, users)

        (
            self._project,
            self._mr,
            self._users,
        ) = init_api()

    def __bool__(self):
        return (
            self._project is not None
            and self._mr is not None
            and self._users is not None
        )

    def get_merge_request_changes(self) -> list:
        @exceptions_decorator_factory(default_value=[])
        def get_changes():
            changes = []
            for chg in self._mr.changes()["changes"]:
                item = data.FileAndLineTuple(chg["new_path"], chg["diff"])
                changes.append(item)
            return changes

        return get_changes()

    def update_merge_request_reviewers(self, reviewers_names: list) -> bool:
        """Add reviewers to merge request"""
        reviewers = set(self._get_users_id(reviewers_names))
        reviewers = set(filter(self._is_user_project_member, reviewers))
        reviewers = reviewers.union(set(self._get_merge_request_reviewers()))

        @exceptions_decorator_factory(default_value=False)
        def get_reviewers():
            self._project.mergerequests.update(
                self._mr.get_id(), {"reviewer_ids": list(reviewers)}
            )
            return True

        return get_reviewers()

    def _is_user_project_member(self, user_id: int) -> bool:
        @exceptions_decorator_factory(default_value=False)
        def check_member():
            return self._project.members_all.get(user_id).attributes["id"] == user_id

        return check_member()

    def _get_merge_request_reviewers(self) -> list:
        @exceptions_decorator_factory(default_value=[])
        def get_reviewers():
            reviewers_list = []
            for reviewer in self._mr.reviewers:
                reviewers_list.append(reviewer["id"])
            return reviewers_list

        return get_reviewers()

    def _get_users_id(self, usernames: list) -> list:
        ids = []
        for name in usernames:
            users_found = self._users.list(username=name)
            if len(users_found) == 0:
                logging.error("No user found with name %s", name)
            elif len(users_found) > 1:
                logging.error("Too many users found with name %s", name)
            else:
                ids.append(users_found[0].get_id())
        return ids
