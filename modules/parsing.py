# Copyright (c) 2014-2024 Zuru Tech HK Limited, All rights reserved.

import logging
import re

from find_deprecations.modules import data

### GCC PARSING ###


def _split(line: str, char: str) -> "data.Maybe":
    splits = re.split(char, line)
    if len(splits) <= 1:
        logging.warning("Cannot split by line number: %s", line)
        return data.Nothing()
    return data.Just(splits)


def _take(container: list, index: int) -> "data.Maybe":
    assert index >= 0
    if index >= len(container):
        logging.warning("Cannot take item at %d in %s", index, str(container))
        return data.Nothing()
    return data.Just(container[index])


def remove_repo_from_file_path(file_abs_apth, repo_path: str) -> "data.Maybe":
    if not repo_path.endswith("/"):
        repo_path = repo_path + "/"
    index = file_abs_apth.find(repo_path)
    if index < 0:
        logging.warning("Cannot find repository path: %s", file_abs_apth)
        return data.Nothing()
    rel_path = file_abs_apth[index + len(repo_path) :]
    return data.Just(rel_path)


def filter_deprecated_warnings(build_output: str) -> list:
    warning_string = "[-Wdeprecated-declarations]"

    def has_warning(line):
        return line.find(warning_string) >= 0

    return list(filter(has_warning, build_output))


LINE_SEPARATOR = ":"


def _find_file_path(line: str) -> "data.Maybe":
    return data.Just(line).map(_split, LINE_SEPARATOR).map(_take, 0)


def _find_line_number(line: str) -> "data.Maybe":
    def convert_to_int(string):
        return data.Just(int(string))

    return data.Just(line).map(_split, LINE_SEPARATOR).map(_take, 1).map(convert_to_int)


def get_deprecated_warnings(build_output_path: str, repo_path: str) -> set:
    with open(build_output_path, "r", encoding="utf-8") as build_output:
        lines = build_output.readlines()
    deprecated_warnings = set()
    for line in filter_deprecated_warnings(lines):
        maybe_file_path = _find_file_path(line).map(
            remove_repo_from_file_path, repo_path
        )
        maybe_line_number = _find_line_number(line)
        if maybe_file_path and maybe_line_number:
            deprecated_warnings.add(
                data.FileAndLineTuple(
                    maybe_file_path.value(), maybe_line_number.value()
                )
            )
    return deprecated_warnings


### GIT DIFF ###


def _read_only_new_lines(line: str) -> "data.Maybe":
    splitted = line.split("+")
    if len(splitted) > 1:
        return data.Just(splitted[1])

    logging.warning("Missing new lines field in %s", line)
    return data.Nothing()


def _read_lines_range(line: str) -> "data.Maybe":
    splitted = line.split(",")
    if len(splitted) > 2:
        logging.warning("Wrong format to read lines number: %s", line)
        return data.Nothing()
    start_line_number = int(splitted[0])
    line_counter = 1
    if len(splitted) == 2:
        line_counter = int(splitted[1])
    return data.Just((start_line_number, line_counter))


def _split_line_and_counter(line: str) -> tuple:
    def remove_white_spaces_at_end(line):
        return data.Just(line.split(" ")[0])

    return (
        data.Just(line)
        .map(_read_only_new_lines)
        .map(remove_white_spaces_at_end)
        .map(_read_lines_range)
    )


def _get_changed_lines(output: str) -> list:
    results = []
    lines = output.split("\n")
    # searching string like: @@ -350,2 +350,5 @@
    # this string points in which lines changes appear
    regex = "\\@\\@ \\-[0-9]+,?[0-9]* \\+[0-9]+,?[0-9]* \\@\\@"
    # Starting from the line which regex matches
    # count following lines of code to find the ones that
    # have been added
    line_counter = 0
    for single_line in lines:
        match_object = re.search(regex, single_line)
        if match_object:
            maybe_starting_line = _split_line_and_counter(match_object.group())
            if maybe_starting_line:
                line_counter = maybe_starting_line.value()[0] - 1
        elif not single_line.startswith("-"):
            line_counter += 1
        if single_line.startswith("+"):
            results.append(line_counter)
    return results


def parse_gitlab_changes(output: list) -> list:
    changes = []
    for file_rel_path, patch in output:
        for value in _get_changed_lines(patch):
            changes.append(data.FileAndLineTuple(file_rel_path, value))
    return changes
