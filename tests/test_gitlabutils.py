# Copyright (c) 2014-2024 Zuru Tech HK Limited, All rights reserved.

from find_deprecations.modules import data
from find_deprecations.modules import parsing

TEST_CHANGES_1 = """
@@ -193,9 +193,11 @@ def get_merge_request_changes() -> list:
         )
         if res.status_code == 200:
             changes = []
+            print("DEBUG: gitlab changes receives")
             for chg in res.json()["changes"]:
                 item = data.FileAndLineTuple(chg["new_path"], chg["diff"])
                 changes.append(item)
+                print(chg["new_path"] + ", " + chg["diff"])
             return changes
     except (
         requests.exceptions.HTTPError,
@@ -264,7 +266,9 @@ def _get_changed_lines(output: str) -> list:

 def parse_gitlab_changes(output: list) -> list:
     changes = []
+    print("DEBUG: changes")
     for file_rel_path, patch in output:
         for value in _get_changed_lines(patch):
             changes.append(data.FileAndLineTuple(file_rel_path, value))
+            print(file_rel_path + ", " + value)
     return changes
"""
TEST_CHANGES_2 = """
@@ -1,3 +1,3 @@
 version https://git-lfs.github.com/spec/v1
-oid sha256:2854a4a1379347ab167126ba71f3e6e99c9278f5af163a8c2bcb1f4d02940445
-size 82123
+oid sha256:c040eb69f29a5891810d4e59473fde026fdada9b26f62365d3604cce6b02febe
+size 95189
"""
TEST_CHANGES_3 = """
@@ -1 +1 @@
-Subproject commit 1aa39b014e14e9108299c3edda656771cbd7b973
+Subproject commit 8f76a7b8e69488dc5379e2e35ec68b6eab581aae
"""
TEST_CHANGES_4 = """
@@ -41,7 +41,7 @@ BEGIN_DEFINE_DC_UI_SPEC(FDCSmartDeviceTests, "BIM.UI.Entities.SmartDevice")
      */
     TWeakObjectPtr<ADCLibraryObject> _spawnSmartDeviceOnWall(FVector wallStartPoint, FVector wallEndPoint);

-    using ExpectationsSetter = TFunction<void(const FString&)>;
+    using ExpectationsSetter = TFunction<void(const FGuid&)>;

     /**
      * \brief Creates a smart device and leaves it in its creation state.
@@ -310,15 +310,16 @@ void FDCSmartDeviceTests::_defineImpl() {
             FVector wallEndPoint = {600.0, wallYPosition, 0.0};
             auto wallEntity = _createWallSync(wallStartPoint, wallEndPoint);

-            auto dcEntity = _spawnSmartDevice([this](const FString& smartDeviceId) {
-                EXPECT_CALL(*_redBoxErrorController, AddErrors(smartDeviceId, testing::_))
-                    .WillOnce([this](const FString& unusedId, const TArray<IEEErrorPtr>& errors) { TestEqual("Number of red box errors", errors.Num(), 2); });
+            auto dcEntity = _spawnSmartDevice([this](const FGuid& smartDeviceGuid) {
+                EXPECT_CALL(*_redBoxErrorController, AddErrors).WillOnce([this](const FGuid& unusedGuid, const TArray<IEEErrorPtr>& errors) {
+                    TestEqual("Number of red box errors", errors.Num(), 2);
+                });

-                EXPECT_CALL(*_mouseTooltipErrorController, AddErrors(smartDeviceId, testing::_)).Times(testing::AtLeast(1));
+                EXPECT_CALL(*_mouseTooltipErrorController, AddErrors).Times(testing::AtLeast(1));
             });
             TEST_NOT_NULLPTR_OR_QUIT_LATENT(dcEntity, doneDelegate);

-            const auto dcEntityId = dcEntity->GetEntityId();
+            const auto dcEntityGuid = dcEntity->TempECSS_GetPimplEntity()->GetGuid();

             DCAutomationTestsUtils::SetViewportMousePositionFromWorldPoint(_editorController, FVector::ZeroVector);
             _automationDriver->Wait(FTimespan(1));
@@ -343,15 +344,15 @@ void FDCSmartDeviceTests::_defineImpl() {

         LatentIt("Should generate a red box error and no tooltip errors if the creation is cancelled", EAsyncExecution::ThreadPool,
             [this](const FDoneDelegate& doneDelegate) {
-                auto dcEntity = _spawnSmartDevice([this](const FString& smartDeviceId) {
-                    EXPECT_CALL(*_redBoxErrorController, AddErrors(smartDeviceId, testing::_))
-                        .WillOnce(
-                            [this](const FString& unusedId, const TArray<IEEErrorPtr>& errors) { TestEqual("Number of red box errors", errors.Num(), 2); });
+                auto dcEntity = _spawnSmartDevice([this](const FGuid& smartDeviceGuid) {
+                    EXPECT_CALL(*_redBoxErrorController, AddErrors).WillOnce([this](const FGuid& smartDeviceGuid, const TArray<IEEErrorPtr>& errors) {
+                        TestEqual("Number of red box errors", errors.Num(), 2);
+                    });

-                    EXPECT_CALL(*_mouseTooltipErrorController, AddErrors(smartDeviceId, testing::_))
+                    EXPECT_CALL(*_mouseTooltipErrorController, AddErrors)
                         .Times(testing::AtLeast(1))
                         .WillRepeatedly(
-                            [this](const FString& /*unusedId*/, const TArray<IEEErrorPtr>& errors) { TestEqual("Tooltip errors count", errors.Num(), 0); });
+                            [this](const FGuid& smartDeviceGuid, const TArray<IEEErrorPtr>& errors) { TestEqual("Tooltip errors count", errors.Num(), 0); });
                 });

                 TEST_NOT_NULLPTR_OR_QUIT_LATENT(dcEntity, doneDelegate);
@@ -378,11 +379,12 @@ void FDCSmartDeviceTests::_defineImpl() {
             FVector wallEndPoint = {wallXPosition, 600.0, 0.0};
             auto wallEntity = _createWallSync(wallStartPoint, wallEndPoint);

-            auto dcEntity = _spawnSmartDevice([this](const FString& smartDeviceId) {
-                EXPECT_CALL(*_redBoxErrorController, AddErrors(smartDeviceId, testing::_))
-                    .WillOnce([this](const FString& unusedId, const TArray<IEEErrorPtr>& errors) { TestEqual("Number of red box errors", errors.Num(), 2); });
+            auto dcEntity = _spawnSmartDevice([this](const FGuid& smartDeviceGuid) {
+                EXPECT_CALL(*_redBoxErrorController, AddErrors).WillOnce([this](const FGuid& unusedGuid, const TArray<IEEErrorPtr>& errors) {
+                    TestEqual("Number of red box errors", errors.Num(), 2);
+                });

-                EXPECT_CALL(*_mouseTooltipErrorController, AddErrors(smartDeviceId, testing::_)).Times(testing::AtLeast(1));
+                EXPECT_CALL(*_mouseTooltipErrorController, AddErrors).Times(testing::AtLeast(1));
             });
             TEST_NOT_NULLPTR_OR_QUIT_LATENT(dcEntity, doneDelegate);

@@ -624,8 +626,11 @@ TWeakObjectPtr<ADCLibraryObject> FDCSmartDeviceTests::_spawnSmartDevice(Expectat
                 return;
             }

+            const FString dcEntityId = dcEntity->GetEntityId();
+            auto libraryObjectController = _entityManager->ResolveEntityController<UDCLibraryObjectController>(dcEntityId);
+
             if (expSetter) {
-                expSetter(dcEntity->GetEntityId());
+                expSetter(dcEntity->TempECSS_GetPimplEntity()->GetGuid());
             }

             const auto assetManager = FDCAssetManagerResolver::GetManager(dcEntity->GetWorld());
@@ -642,11 +647,8 @@ TWeakObjectPtr<ADCLibraryObject> FDCSmartDeviceTests::_spawnSmartDevice(Expectat
             FDCEntity_Asset assetInfo = {};
             assetInfo.Id = _MEPAssetGuid;

-            const FString dcEntityId{dcEntity->GetEntityId()};
-
             assetManager->LoadAssetAsyncByEntityAsset(
-                assetInfo, [this, libraryObjectController = _entityManager->ResolveEntityController<UDCLibraryObjectController>(dcEntityId),
-                               assetLoaderLogicCWeakPtr, libObjWeakPtr, expSetter](FAssetLoadedEventArgs assetLoadedEventArgs) {
+                assetInfo, [this, libraryObjectController, assetLoaderLogicCWeakPtr, libObjWeakPtr, expSetter](FAssetLoadedEventArgs assetLoadedEventArgs) {
                     libraryObjectController->OnAssetLoaded(assetLoadedEventArgs, assetLoaderLogicCWeakPtr, libObjWeakPtr, true);

                     // Set the errors' controllers of the smart device editing mode.
"""


def test_parse_one():
    changes = parsing.parse_gitlab_changes(
        [
            data.FileAndLineTuple(
                ".gitlab/scripts/find_deprecations/modules/gitlab_utils.py",
                TEST_CHANGES_1,
            )
        ]
    )
    assert changes[0].line == 196
    assert changes[1].line == 200
    assert changes[2].line == 269
    assert changes[3].line == 273


def test_parse_two():
    changes = parsing.parse_gitlab_changes(
        [
            data.FileAndLineTuple(
                "Dreamcatcher/Content/Blueprints/WBP_FactoryErrorsNotifier_EntityItem.uasset",
                TEST_CHANGES_2,
            )
        ]
    )
    assert changes[0].line == 2
    assert changes[1].line == 3


def test_parse_three():
    changes = parsing.parse_gitlab_changes(
        [
            data.FileAndLineTuple(
                "Dreamcatcher/Plugins/EntityComponentSS", TEST_CHANGES_3
            )
        ]
    )
    assert changes[0].line == 1


def test_parse_four():
    changes = parsing.parse_gitlab_changes(
        [
            data.FileAndLineTuple(
                "Dreamcatcher/Source/DCAutomationTests/Private/Source/UI/Entities/SmartDevice.spec.cpp",
                TEST_CHANGES_4,
            )
        ]
    )
    assert changes[0].line == 44
    assert changes[1].line == 313
    assert changes[2].line == 314
    assert changes[3].line == 315
    assert changes[4].line == 316
    assert changes[5].line == 318
    assert changes[6].line == 322
    assert changes[7].line == 347
    assert changes[8].line == 348
    assert changes[9].line == 349
    assert changes[10].line == 350
    assert changes[11].line == 352
    assert changes[12].line == 355
    assert changes[13].line == 382
    assert changes[14].line == 383
    assert changes[15].line == 384
    assert changes[16].line == 385
    assert changes[17].line == 387
    assert changes[18].line == 629
    assert changes[19].line == 630
    assert changes[20].line == 631
    assert changes[21].line == 633
    assert changes[22].line == 651
