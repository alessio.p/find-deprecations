# Find deprecated functions usage

### Motivation

We need to avoid brand new code that uses deprecated functions. Otherwise refactoring will never end.

This script reads GCC compile output, looking for deprecated function warnings. Then it queries the local Git repository to get all the differences introduced
by the working branch since the base branch. By matching file name and line number of deprecation warnings with ones returned by git diff, it checks that
the working branch doesn't use any deprecated functions.

### Hot to run

python find_deprecations/main.py GCC_OUTPUT REPO_ABS_PATH START_COMMIT_SHA END_COMMIT_SHA

- GCC_OUTPUT: the path of the file that contains the build output. Only gcc format is supported
- REPO_ABS_PATH: the absolute path of the git repository
- START_COMMIT_SHA: the first commit passed to git diff
- END_COMMIT_SHA: the second commit passed to git diff
- DEBUG_REPO: to debug in local, this path is used to query git repo
